import cats.effect._
import cats.syntax.all._

object App extends IOApp {
  import com.benmosheron.sound.AudioFilePlayer
  def run(args: List[String]): IO[ExitCode] = 
    args.headOption match {
      case None     => noFile
      case Some("") => noFile
      case Some(filePath) => IO(AudioFilePlayer.run(filePath)).as(ExitCode.Success)
    }

  def noFile: IO[ExitCode] = IO(System.err.println("No file path specified")).as(ExitCode(2))
}
