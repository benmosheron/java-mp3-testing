Attempting to get mp3 stream working using old skool, command line java,
so I can understand what needs to happen with the classpath and hopefully get it working in scala.

See source-functions.sh for compile and run functions.

Java and scala compilation & running works fine, but using SBT causes stuff to break.
I think this is because SBT uses a different class loader.

Instructions:
1) CD to project root.
2) `$ source source-functions.sh`
3)
  a) To run via java: `$ compile-java && run-java "/path/to/my.mp3"`
  b) To run via scala: `$ compile && run "/path/to/my.mp3"`
