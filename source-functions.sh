# source this file to gain compile and root functions
# run me from root

JAVA_DEPS=lib/tritonus-share-0.3.7-2.jar:lib/mp3spi-1.9.5-1.jar:lib/jlayer-1.0.1-1.jar
SCALA_DEPS=$HOME/Dev/Scala/lib/cats-kernel_2.12-1.4.0.jar:$HOME/Dev/Scala/lib/cats-core_2.12-1.4.0.jar:$HOME/Dev/Scala/lib/cats-effect_2.12-1.0.0.jar

function compile-java {
  # Java compile has no dependencies - we just have one source file. Easy peasy.
  javac -d javac-out src/main/java/com/benmosheron/sound/AudioFilePlayer.java
}

# Argument:
# $1 = Path to mp3 file
function run-java {
  # We have three runtime dependencies which must be on the class path, along with our actual class in javac-out
  java -cp $JAVA_DEPS:javac-out com.benmosheron.sound.AudioFilePlayer "$1"
}

function compile {
  # Compile java sources
  echo "Compiling java source"
  compile-java
  # compile scala source, with java classes on the classpath
  echo "Compiling scala source"
  scalac -d scalac-out -cp $SCALA_DEPS:javac-out:.  src/main/scala/App.scala
}

# Argument:
# $1 = Path to mp3 file
function run {
  scala -cp $JAVA_DEPS:$SCALA_DEPS:javac-out:scalac-out App "$1"
}
